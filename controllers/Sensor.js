'use strict';

var url = require('url');

var Sensor = require('./SensorService');

module.exports.addSensor = function addSensor (req, res, next) {
  Sensor.addSensor(req.swagger.params, res, next);
};

module.exports.addSensorPeers = function addSensorPeers (req, res, next) {
  Sensor.addSensorPeers(req.swagger.params, res, next);
};

module.exports.getSensor = function getSensor (req, res, next) {
  Sensor.getSensor(req.swagger.params, res, next);
};

module.exports.getSensorSnapshot = function getSensorSnapshot (req, res, next) {
  Sensor.getSensorSnapshot(req.swagger.params, res, next);
};

module.exports.getSensors = function getSensors (req, res, next) {
  Sensor.getSensors(req.swagger.params, res, next);
};

module.exports.removeSensor = function removeSensor (req, res, next) {
  Sensor.removeSensor(req.swagger.params, res, next);
};

module.exports.removeSensorPeer = function removeSensorPeer (req, res, next) {
  Sensor.removeSensorPeer(req.swagger.params, res, next);
};

module.exports.updateSensor = function updateSensor (req, res, next) {
  Sensor.updateSensor(req.swagger.params, res, next);
};

module.exports.updateSensorSnapshot = function updateSensorSnapshot (req, res, next) {
  Sensor.updateSensorSnapshot(req.swagger.params, res, next);
};
