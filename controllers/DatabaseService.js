'use strict';

exports.addDatabase = function(args, res, next) {
  /**
   * Set up a database
   * 
   *
   * body DatabaseParameters The new database to create
   * no response value expected for this operation
   **/
  res.end();
}

exports.addNodeToDatabase = function(args, res, next) {
  /**
   * Add node(s) to the database cluster
   * 
   *
   * name String A unique name for the database
   * body List A list of nodes
   * no response value expected for this operation
   **/
  res.end();
}

exports.getDatabase = function(args, res, next) {
  /**
   * Get information of the database
   * 
   *
   * name String A unique name for the database
   * returns Database
   **/
  var examples = {};
  examples['application/json'] = {
  "connectionString" : "aeiou",
  "password" : "aeiou",
  "port" : 123,
  "name" : "aeiou",
  "creationTimestamp" : "2000-01-23T04:56:07.000+00:00",
  "hostnames" : [ "aeiou" ],
  "type" : "aeiou",
  "username" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getDatabaseBackupFile = function(args, res, next) {
  /**
   * Download a backup of the database
   * 
   *
   * name String A unique name for the database
   * no response value expected for this operation
   **/
  res.end();
}

exports.getDatabases = function(args, res, next) {
  /**
   * Get a list of created database
   * 
   *
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "connectionString" : "aeiou",
  "password" : "aeiou",
  "port" : 123,
  "name" : "aeiou",
  "creationTimestamp" : "2000-01-23T04:56:07.000+00:00",
  "hostnames" : [ "aeiou" ],
  "type" : "aeiou",
  "username" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.removeDatabase = function(args, res, next) {
  /**
   * Delete the database
   * 
   *
   * name String A unique name for the database
   * no response value expected for this operation
   **/
  res.end();
}

exports.removeNodeFromDatabase = function(args, res, next) {
  /**
   * Remove a node from the database cluster
   * 
   *
   * name String A unique name for the database
   * hostname String hostname of the name
   * no response value expected for this operation
   **/
  res.end();
}

exports.restoreDatabase = function(args, res, next) {
  /**
   * Restore the database
   * 
   *
   * name String A unique name for the database
   * file File A database backup file
   * no response value expected for this operation
   **/
  res.end();
}

