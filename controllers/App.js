'use strict';

var url = require('url');

var App = require('./AppService');

module.exports.getApp = function getApp (req, res, next) {
  App.getApp(req.swagger.params, res, next);
};

module.exports.getAppBundle = function getAppBundle (req, res, next) {
  App.getAppBundle(req.swagger.params, res, next);
};

module.exports.getAppReadme = function getAppReadme (req, res, next) {
  App.getAppReadme(req.swagger.params, res, next);
};

module.exports.getAppType = function getAppType (req, res, next) {
  App.getAppType(req.swagger.params, res, next);
};

module.exports.getApps = function getApps (req, res, next) {
  App.getApps(req.swagger.params, res, next);
};

module.exports.getConfigurationTemplate = function getConfigurationTemplate (req, res, next) {
  App.getConfigurationTemplate(req.swagger.params, res, next);
};

module.exports.installApp = function installApp (req, res, next) {
  App.installApp(req.swagger.params, res, next);
};

module.exports.removeApp = function removeApp (req, res, next) {
  App.removeApp(req.swagger.params, res, next);
};

module.exports.updateApp = function updateApp (req, res, next) {
  App.updateApp(req.swagger.params, res, next);
};
