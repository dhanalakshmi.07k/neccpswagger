'use strict';

exports.backupSystem = function(args, res, next) {
  /**
   * Download a backup of the execution framework database
   * 
   *
   * no response value expected for this operation
   **/
  res.end();
}

exports.restoreSystem = function(args, res, next) {
  /**
   * Restore the execution framework database
   * 
   *
   * file File execution framework database backup file
   * no response value expected for this operation
   **/
  res.end();
}

