'use strict';

var url = require('url');

var User = require('./UserService');

module.exports.addAccount = function addAccount (req, res, next) {
  User.addAccount(req.swagger.params, res, next);
};

module.exports.getAccounts = function getAccounts (req, res, next) {
  User.getAccounts(req.swagger.params, res, next);
};

module.exports.removeAccount = function removeAccount (req, res, next) {
  User.removeAccount(req.swagger.params, res, next);
};

module.exports.updateAccount = function updateAccount (req, res, next) {
  User.updateAccount(req.swagger.params, res, next);
};
