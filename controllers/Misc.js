'use strict';

var url = require('url');

var Misc = require('./MiscService');

module.exports.getVersion = function getVersion (req, res, next) {
  Misc.getVersion(req.swagger.params, res, next);
};
