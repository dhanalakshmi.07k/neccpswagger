'use strict';
var low	= require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter);
// Load the full build. 
var _ = require('lodash');
// Load the core build. 
var _ = require('lodash/core');
exports.addSensor = function(args, res, next) {
  /**
   * Add a new sensor
   * 
   *
   * body SensorParameters The new sensor
   * returns SensorBasicInfo
   **/
  var examples = {};
  examples['application/json'] = {
  "locationId" : 123456789,
  "name" : "aeiou",
  "id" : 123456789,
  "type" : "aeiou",
  "vms" : "aeiou",
  "uri" : "aeiou",
  "info" : {
    "key" : "{}"
  }
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.addSensorPeers = function(args, res, next) {
  /**
   * Add a list of sensors to work with this sensor
   * 
   *
   * sensorId Long Unique ID of the sensor
   * body List A list of sensor IDs
   * no response value expected for this operation
   **/
  res.end();
}

exports.getSensor = function(args, res, next) {
  /**
   * Retrieve a sensor
   * 
   *
   * sensorId Long Unique ID of the sensor
   * returns Sensor
   **/
	console.log("getInstance method");
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('sensor').value();
	if(args.sensorId.value ===undefined)
		console.log("object undefined");
   if(args.sensorId.value !== undefined) {
	    result = _.filter(result,{id: args.sensorId.value});
	  
	}
	
	
	 res.end(JSON.stringify(result));
}

exports.getSensorSnapshot = function(args, res, next) {
  /**
   * Get the most recent snapshot of the sensor
   * 
   *
   * sensorId Long Unique ID of the sensor
   * returns SensorSnapshot
   **/
  var examples = {};
  examples['application/json'] = {
  "time" : "2000-01-23T04:56:07.000+00:00",
  "snapshot" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getSensors = function(args, res, next) {
  /**
   * Retrieve all sensors
   * 
   *
   * type String Type of the sensor, e.g., camera. If specified, list only sensors of the specific type. (optional)
   * vms String Name of the VMS vendor if the stream is from VMS, e.g., genetec and milestone (optional)
   * locationId Long Filter out results by locationId. -1 means showing all results. (optional)
   * instanceId Long Only show sensors used by the given instance. -1 means showing all sensors. (optional)
   * appId String Only show sensors used by any instance of the given app. -1 means showing all sensors. (optional)
   * returns List
   **/
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('sensor').value();

   if(args.type.value !== undefined) {
	   console.log("args.type.value="+args.type.value);
	    result = _.filter(result, {type: args.type.value});
	}
	
	 if(args.vms.value !== undefined) {
		 console.log("args.vms.value="+args.vms.value);
	      result = _.filter(result, {vms: args.vms.value});
	}
	 if(args.locationId.value !== undefined && args.locationId.value > 0) {
		 console.log("args.locationId.value="+args.locationId.value);
	    result = _.filter(result, {locationId: args.locationId.value});
	}
		 if(args.instanceId.value !== undefined  && args.instanceId.value > 0) {
		 console.log("args.instanceId.value="+args.instanceId.value);
	      result = _.filter(result, {instanceId: args.instanceId.value});
	}
	 if(args.appId.value !== undefined) {
		 console.log("args.appId.value="+args.appId.value);
	    result = _.filter(result, {appId: args.appId.value});
	}
	console.log("result"+result);
	 res.end(JSON.stringify(result));
}

exports.removeSensor = function(args, res, next) {
  /**
   * Remove a sensor
   * 
   *
   * sensorId Long Unique ID of the sensor
   * no response value expected for this operation
   **/
  res.end();
}

exports.removeSensorPeer = function(args, res, next) {
  /**
   * Remove a sensor as a peer of another sensor
   * 
   *
   * sensorId Long Unique ID of the sensor
   * peerId Long sensorId of the peer
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateSensor = function(args, res, next) {
  /**
   * Update the configuration and information of a sensor
   * 
   *
   * sensorId Long Unique ID of the sensor
   * body SensorParameters Updated parameters of the sensor
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateSensorSnapshot = function(args, res, next) {
  /**
   * Update the snapshot of the sensor (FOR INTERNAL USE ONLY)
   * 
   *
   * sensorId Long Unique ID of the sensor
   * body SensorSnapshot Snapshot from the sensor
   * no response value expected for this operation
   **/
  res.end();
}

