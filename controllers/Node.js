'use strict';

var url = require('url');

var Node = require('./NodeService');

module.exports.deleteNode = function deleteNode (req, res, next) {
  Node.deleteNode(req.swagger.params, res, next);
};

module.exports.getClient = function getClient (req, res, next) {
  Node.getClient(req.swagger.params, res, next);
};

module.exports.getFluentAggregatorConfiguration = function getFluentAggregatorConfiguration (req, res, next) {
  Node.getFluentAggregatorConfiguration(req.swagger.params, res, next);
};

module.exports.getFluentConfiguration = function getFluentConfiguration (req, res, next) {
  Node.getFluentConfiguration(req.swagger.params, res, next);
};

module.exports.getNodes = function getNodes (req, res, next) {
  Node.getNodes(req.swagger.params, res, next);
};

module.exports.getRegistryCredential = function getRegistryCredential (req, res, next) {
  Node.getRegistryCredential(req.swagger.params, res, next);
};

module.exports.getSslCertificate = function getSslCertificate (req, res, next) {
  Node.getSslCertificate(req.swagger.params, res, next);
};

module.exports.getWindowsServiceTemplate = function getWindowsServiceTemplate (req, res, next) {
  Node.getWindowsServiceTemplate(req.swagger.params, res, next);
};

module.exports.setNodeInfo = function setNodeInfo (req, res, next) {
  Node.setNodeInfo(req.swagger.params, res, next);
};

module.exports.setNodeLocation = function setNodeLocation (req, res, next) {
  Node.setNodeLocation(req.swagger.params, res, next);
};

module.exports.updateNode = function updateNode (req, res, next) {
  Node.updateNode(req.swagger.params, res, next);
};
